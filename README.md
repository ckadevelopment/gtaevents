# GtaEvents

Gathers online events from Rockstar's Social Club site. Also features filterable contact mission stats.

## Install GtaEvents
### Add GtaEvents folder to path
GtaEvents uses selenium to access Rockstar Social Club website, and chromedriver needs to be added to<br>
users path to function. (Gui will not run unless chromedriver is in the users path)<br>
'Win-key' -> type 'env' -> select enviroment editor -> add gtaEvents folder to users path.<br>

### Install Pricedown font
Right-click and install Pricedown ttf font.

### Install Hack font
Right-click and install Hack ttf fonts.

### Run GtaEvents
Click on gtaEvents.exe to start GtaEvents application.
